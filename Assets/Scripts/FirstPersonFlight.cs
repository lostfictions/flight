using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FirstPersonFlight : MonoBehaviour
{
	public float forwardSpeed = 10f;
	public float turnSpeed = 1f;

	public float bankAmount = 2f;
	// public float maxBankRate = 3f;

	public Transform camTransform;

	CharacterController cc;

	void Start()
	{
		cc = GetComponent<CharacterController>();
	}
	
	void Update()
	{
		float inputTurn = Input.GetAxis("Horizontal");
		float inputForward = Input.GetAxis("Vertical");

		float forwardVelocity = forwardSpeed * inputForward;
		float turnAmount = inputTurn * turnSpeed * Time.deltaTime;


		float verticalAdjustment = 0;

		RaycastHit hit;
		if(Physics.Raycast(transform.position, -Vector3.up, out hit, cc.height))
		{
			float distanceToGround = hit.distance - cc.height / 2f;
			if(distanceToGround < 0.5f)
			{
				//Should be accelerative/eased based on how close to the earth we are?
				verticalAdjustment = 0.5f;
			}
		}

		transform.Rotate(0, turnAmount, 0);

		// float deltaTurn = Mathy.DirectionalDeltaAngle(lastTurnAmount, turnAmount);
		// Debug.Log(deltaTurn);

		// Vector3 camRot = camTransform.localEulerAngles;
		// float targetAngle = turnAmount * bankAmount;
		// camRot.z = Mathf.MoveTowardsAngle(camRot.z, -targetAngle, );
		// camTransform.localEulerAngles = camRot;

		// float targetAngle = inputTurn * maxBankAmount * inputForward;
		// camRot.z = Mathf.MoveTowardsAngle(camRot.z, targetAngle, targetAngle > camRot.z ? maxBankRate : maxBankRate * 10f);


		// Vector3 forward = transform.TransformDirection(Vector3.forward);

		// float curSpeed = speed * Input.GetAxis("Vertical");


		Vector3 moveVector = transform.TransformDirection(new Vector3(0, verticalAdjustment, forwardVelocity * Time.deltaTime));

		cc.Move(moveVector);
	}
}
